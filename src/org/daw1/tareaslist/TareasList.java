/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.tareaslist;

import java.util.List;
import java.util.LinkedList;
/**
 *
 * @author rgcenteno
 */
public class TareasList {

    final static java.util.Scanner teclado = new java.util.Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String opcion = "";
        do{     
            System.out.println("************************************************");
            System.out.println("* 1. Ejercicio 1                               *");
            System.out.println("* 2. Ejercicio 2                               *");
            System.out.println("*                                              *");
            System.out.println("* 0. Salir                                     *");
            System.out.println("************************************************");
            opcion = teclado.nextLine();
            switch(opcion){
                case "1":
                    programa1();
                    break;
                case "2":
                    programa2();
                    break;
            }
        }
        while(!opcion.equals("0"));
    }
    
    private static void programa1(){
        List<String> listaPalabras = new LinkedList<String>();
        String palabra = "";
        do{
            System.out.println("Inserte un nombre. Escriba \"final\" para terminar.");
            palabra = teclado.nextLine();
            if(palabra.length() > 0 && !palabra.equals("final")){
                listaPalabras.add(palabra);            
            }
            System.out.println(listaPalabras);
        }
        while(!palabra.equals("final"));
        String opcion = "";
        do{     
            System.out.println(listaPalabras);
            System.out.println("\n**********************************************");
            System.out.println("* 1. Comprobar existencia nombre               *");
            System.out.println("* 2. Borrar nombre                             *");
            System.out.println("* 3. Borrar nombres contienen texto            *");
            System.out.println("* 4. Ver elemento posición                     *");
            System.out.println("* 5. Reemplazar primera aparición              *");
            System.out.println("*                                              *");
            System.out.println("* 0. Salir                                     *");
            System.out.println("************************************************");
            opcion = teclado.nextLine();
            switch(opcion){
                case "1":
                    System.out.println("Inserte la palabra a buscar");
                    palabra = teclado.nextLine();
                    System.out.printf("La palabra \"%s\" %s existe en la lista.\n\n", palabra, listaPalabras.contains(palabra) ? "SÍ" : "NO");
                    break;
                case "2":
                    System.out.println("Inserte la palabra a eliminar");
                    palabra = teclado.nextLine();
                    int borradas = 0;
                    while(listaPalabras.remove(palabra)){
                        borradas++;
                    }
                    if(borradas > 0){
                        System.out.println("Se ha borrado " + borradas + " la palabra " + palabra);
                    }
                    else{
                        System.out.println("No se ha borrado la palabra: "+ palabra);
                    }
                    break;
                case "3":
                    System.out.println("Inserte la palabra que deben contener los elementos a borrar");
                    do{
                        palabra = teclado.nextLine();
                    }while(palabra.length() == 0);
                    
                    final String p = palabra;
                    //listaPalabras.removeIf(actual -> actual.contains(p));
                    java.util.Iterator<String> it = listaPalabras.iterator();
                    while (it.hasNext()) {
                        String next = it.next();
                        if(next.contains(palabra)){
                            it.remove();
                        }
                    }
                    break;
                case "4":                    
                    System.out.println("Por favor inserte la posición que desea revisar.");
                    int pos = -1;
                    do{
                        if(teclado.hasNextInt()){
                            pos = teclado.nextInt();
                            if(pos >= 0 && pos < listaPalabras.size()){
                                System.out.printf("El elemento en la posición %d es \"%s\"\n", pos, listaPalabras.get(pos));
                            }
                            else{
                                System.out.printf("Escriba un número entre 0 y %d\n", listaPalabras.size());
                            }
                        }
                        else{
                            System.out.println("Inserte un número");
                        }
                        teclado.nextLine();
                    }
                    while(pos < 0 || pos >= listaPalabras.size());  
                    break;
                case "5":
                    String palabra1 = "";
                    do{
                        System.out.println("Por favor inserte la palabra que desea reemplazar");
                        palabra1 = teclado.nextLine();
                    }
                    while(palabra1.isEmpty());
                    String palabra2 = "";
                    do{
                        System.out.println("Por favor inserte la palabra que se pondrá en su lugar");
                        palabra2 = teclado.nextLine();
                    }
                    while(palabra2.isEmpty());
                    int indice = listaPalabras.indexOf(palabra1);
                    if(indice == -1){
                        System.out.println("La palabra " + palabra1 + " no se encuentra en la lista");
                    }
                    else{
                        listaPalabras.set(indice, palabra2);
                    }            
                    break;
                case "6":
                    palabra1 = "";
                    do{
                        System.out.println("Por favor inserte la palabra que desea reemplazar");
                        palabra1 = teclado.nextLine();
                    }
                    while(palabra1.isEmpty());
                    palabra2 = "";
                    do{
                        System.out.println("Por favor inserte la palabra que se pondrá en su lugar");
                        palabra2 = teclado.nextLine();
                    }
                    while(palabra2.isEmpty());
                    int sustituidas = 0;                    
                    indice = listaPalabras.indexOf(palabra1);
                    while(indice != -1){
                        listaPalabras.set(indice, palabra2);
                        sustituidas++;
                        indice = listaPalabras.indexOf(palabra1);
                    }
                    if(sustituidas == 0){
                        System.out.println("La palabra " + palabra1 + " no existía en la lista");
                    }
                    else{
                        System.out.println("Se han realizado " + sustituidas + " sustituciones");
                    }
                    break;
                case "7":
                    System.out.println(listaPalabras);
                    break;
                case "0":
                    break;
                default:
                    System.out.println("Opción inválida");
                
            }
        }while(!opcion.equals("0"));     
    }
    
    private static void programa2(){
        String frase1 = "";
        do{
            System.out.println("Inserte la primera frase");
            frase1 = teclado.nextLine();
        }
        while(frase1.isEmpty());
        String frase2 = "";
        do{
            System.out.println("Inserte la segunda frase");
            frase2 = teclado.nextLine();
        }
        while(frase2.isEmpty());
        String[] array1 = frase1.split(" +");
        List<String> lista1 = java.util.Arrays.asList(array1);
        List<String> lista2 = java.util.Arrays.asList(frase2.split(" +"));
        
        System.out.println("Palabras comunes: \n" + interseccion(lista1, lista2) );
        System.out.println("Palabras NO comunes: \n" + noComunes(lista1, lista2) );
        
        
    }
    
    private static List<String> interseccion(List<String> l1, List<String> l2){
        List<String> noComun = new java.util.ArrayList<String>(l1);
        noComun.removeAll(l2);
        
        List<String> comun = new java.util.ArrayList<String>(l1);
        comun.removeAll(noComun);
        return comun;
    }
    
    private static List<String> noComunes(List<String> l1, List<String> l2){
        List<String> noComun1 = new java.util.ArrayList<String>(l1);
        noComun1.removeAll(l2);
        
        List<String> noComun2 = new java.util.ArrayList<String>(l2);
        noComun2.removeAll(l1);
        
        noComun1.addAll(noComun2);
        return noComun1;
    }
    
}
